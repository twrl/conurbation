## Logging helpers

function error () {
    echo "$(tput setaf 1)$(tput bold)ERROR:$(tput sgr0)    ${1}"
    exit 1
}

function warn () {
    echo "$(tput setaf 3)Warning:$(tput sgr0)  ${1}"
}

function ok() {
    echo "$(tput setaf 2)OK:$(tput sgr0)       ${1}"
}

function log () {
    echo "          ${1}"
}
