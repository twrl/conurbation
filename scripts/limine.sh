function download_limine_blob() {

    curl -L https://github.com/limine-bootloader/limine/archive/refs/heads/v4.x-branch-binary.zip --etag-save "${CONURBATION_BINARY_DIR}/blobs/limine.zip.etag" --etag-compare "${CONURBATION_BINARY_DIR}/blobs/limine.zip.etag" -o "${CONURBATION_BINARY_DIR}/blobs/limine.zip"

    mkdir -p "${CONURBATION_BINARY_DIR}/blobs/limine"
    unzip -o -j "${CONURBATION_BINARY_DIR}/blobs/limine.zip" limine-4.x-branch-binary/BOOTX64.EFI -d "${CONURBATION_BINARY_DIR}/blobs/limine"
}