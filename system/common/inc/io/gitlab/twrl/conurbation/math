// -*- C++ -*-
#pragma once

#include <io/gitlab/twrl/conurbation/scalars>

extern "C" {
    // IEEE 754 primitives are extern "C" and live in ::

    auto __ieee754_exp(io::gitlab::twrl::conurbation::float64_t x) -> io::gitlab::twrl::conurbation::float64_t;
    auto __ieee754_expf(io::gitlab::twrl::conurbation::float32_t x) -> io::gitlab::twrl::conurbation::float32_t;
    auto __ieee754_log(io::gitlab::twrl::conurbation::float64_t x) -> io::gitlab::twrl::conurbation::float64_t;
    auto __ieee754_logf(io::gitlab::twrl::conurbation::float32_t x) -> io::gitlab::twrl::conurbation::float32_t;
    auto __ieee754_pow(io::gitlab::twrl::conurbation::float64_t x, io::gitlab::twrl::conurbation::float64_t y) -> io::gitlab::twrl::conurbation::float64_t;
    auto __ieee754_powf(io::gitlab::twrl::conurbation::float32_t x, io::gitlab::twrl::conurbation::float32_t y) -> io::gitlab::twrl::conurbation::float32_t;
    auto __ieee754_sqrt(io::gitlab::twrl::conurbation::float64_t x) -> io::gitlab::twrl::conurbation::float64_t;
    auto __ieee754_sqrtf(io::gitlab::twrl::conurbation::float32_t x) -> io::gitlab::twrl::conurbation::float32_t;

}

namespace io::gitlab::twrl::conurbation {
    
    template <class T>
    inline auto abs(T value) noexcept -> T {
        return value < 0 ? -value : value;
    }

    template <class T>
    inline auto max(T a, T b) noexcept -> T {
        return a > b ? a : b;
    }

    template <class T>
    inline auto min(T a, T b) noexcept -> T {
        return a < b ? a : b;
    }

    inline auto pow(float32_t x, float32_t y) noexcept -> float32_t {
        return __ieee754_powf(x, y);
    }

    inline auto pow(float64_t x, float64_t y) noexcept -> float64_t {
        return __ieee754_pow(x, y);
    }

    inline auto sqrt(float32_t x) noexcept -> float32_t {
        return __ieee754_sqrtf(x);
    }

    inline auto sqrt(float64_t x) noexcept -> float64_t {
        return __ieee754_sqrt(x);
    }

    auto ceil(float32_t) noexcept -> float32_t;
    auto ceil(float64_t) noexcept -> float64_t;

    auto scalbn(float32_t, int32_t) noexcept -> float32_t;
    auto scalbn(float64_t, int32_t) noexcept -> float64_t;

    auto copysign(float32_t, float32_t) noexcept -> float32_t;
    auto copysign(float64_t, float64_t) noexcept -> float64_t;


}