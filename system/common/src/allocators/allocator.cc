#include <io/gitlab/twrl/conurbation/allocators/allocator>

using namespace io::gitlab::twrl::conurbation;
using namespace io::gitlab::twrl::conurbation::allocators;

auto allocator_t::allocate(size_t size, size_t align) -> memory_t {
    return this->do_allocate(size, align);
}
auto allocator_t::obtain(void* ptr) -> memory_t {
    return this->do_obtain(ptr);
}
auto allocator_t::deallocate(void* ptr) -> void {
    return this->do_deallocate(this->do_obtain(ptr));
}
auto allocator_t::deallocate(memory_t mem) -> void {
    return this->do_deallocate(mem);
}

auto allocator_t::add_child(allocator_t* child) noexcept -> void {
    child->parent(this);

    if ((children_max_ - children_count_) <= 1) {
        size_t nchildren = children_max_;
        if (nchildren == 0) {
            nchildren = 64 / sizeof(allocator_t*);
        } else {
            nchildren *= 2;
        }

        memory_t new_children;
        memory_t old_children;
        new_children = root_->allocate(nchildren * sizeof(allocator_t*), alignof(allocator_t*));
        old_children = root_->obtain(children_);

        new_children.copy_from(old_children);

        children_ = reinterpret_cast<allocator_t**>(new_children.base());
        children_max_ = nchildren;

        root_->deallocate(old_children);

    }

    children_[children_count_++] = child;

}

void* operator new (io::gitlab::twrl::conurbation::size_t size, io::gitlab::twrl::conurbation::allocators::allocator_t& alloc) {
    return reinterpret_cast<void*>(alloc.allocate(size, 1).base());
}