#include <io/gitlab/twrl/conurbation/allocators/passthru>

using namespace io::gitlab::twrl::conurbation;
using namespace io::gitlab::twrl::conurbation::allocators;

auto passthru_t::do_allocate(size_t size, size_t align) noexcept -> memory_t {
    if (parent()) return parent()->allocate(size, align);
    else return memory_t();
}

auto passthru_t::do_obtain(void* ptr) noexcept -> memory_t {
    if (parent()) return parent()->obtain(ptr);
    else return memory_t();
}

auto passthru_t::do_deallocate(memory_t mem) noexcept -> void {
    if (parent()) parent()->deallocate(mem);
}