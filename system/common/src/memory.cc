#include <io/gitlab/twrl/conurbation/math>
#include <io/gitlab/twrl/conurbation/memory>

using namespace io::gitlab::twrl::conurbation;

auto memory_t::copy_from(memory_t& other) noexcept -> memory_t& {

    size_t len = min(size(), other.size());

    const uint8_t* src = reinterpret_cast<const uint8_t*>(other.base());
    uint8_t* dest = reinterpret_cast<uint8_t*>(base());

    for (int i = 0; i < len; ++i)
    {
        dest[i] = src[i];
    }

    return *this;
}

auto memory_t::fill(uint8_t value) noexcept -> memory_t& {

    size_t len = size();

    uint8_t* dest = reinterpret_cast<uint8_t*>(base());

    for (int i = 0; i < len; ++i)
    {
        dest[i] = value;    
    }

    return *this;
}

auto memory_t::fill(uint16_t value) noexcept -> memory_t& {

    size_t len = size() / sizeof(uint16_t);

    uint16_t* dest = reinterpret_cast<uint16_t*>(base());

    for (int i = 0; i < len; ++i)
    {
        dest[i] = value;
    }

    return *this;
}

auto memory_t::fill(uint32_t value) noexcept -> memory_t& {

    size_t len = size() / sizeof(uint32_t);

    uint32_t* dest = reinterpret_cast<uint32_t*>(base());

    for (int i = 0; i < len; ++i)
    {
        dest[i] = value;
    }

    return *this;
}

auto memory_t::fill(uint64_t value) noexcept -> memory_t& {

    size_t len = size() / sizeof(uint64_t);

    uint64_t* dest = reinterpret_cast<uint64_t*>(base());

    for (int i = 0; i < len; ++i)
    {
        dest[i] = value;
    }

    return *this;

}