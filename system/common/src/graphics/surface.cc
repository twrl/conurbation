#include <io/gitlab/twrl/conurbation/graphics/surface>

using namespace io::gitlab::twrl::conurbation;
using namespace io::gitlab::twrl::conurbation::graphics;

namespace {

    static inline auto _line_bisect_draw(surface_p& surface, line_t& l, colour_t& c) -> void {

            point_t a = l.evaluate_at(0.0f);
            point_t b = l.evaluate_at(1.0f);

            const bool steep = (abs(b.y - a.y) > abs(b.x > a.x));
            float32_t x1, y1, x2, y2;
            if (steep) {
                x1 = a.y;
                y1 = a.x;
                x2 = b.y;
                y2 = b.x;
            } else {
                x1 = a.x;
                y1 = a.y;
                x2 = b.x;
                y2 = b.y;
            }

            if (x1 > x2) {
                swap(x1, x2);
                swap(y1, y2);
            }

            const float32_t dx = x2 - x1;
            const float32_t dy = abs(y2 - y1);

            float32_t error = dx / 2.0f;
            const int32_t ystep = (y1 < y2) ? 1 : -1;
            int32_t y = int32_t(y1);

            const int32_t xmax = int32_t(x2);
            for (int32_t x = int32_t(x1); x < xmax; ++x) {
                if (steep) surface.draw(point_t(y, x), c);
                else surface.draw(point_t(x, y), c);

                error -= dy;
                if (error < 0) {
                    y += ystep;
                    error += dx;
                }
            }

        }

        static inline auto _quad_bisect_draw(surface_p& surface, quad_bezier_t& q, colour_t& c, float32_t a, float32_t b) -> void {
            if (q.versine(a, b) <= 1.0) {
                surface.draw(q.evaluate_at(a), q.evaluate_at(b), c);
            } else {
                _quad_bisect_draw(surface, q, c, a, (a+b)/2.0);
                _quad_bisect_draw(surface, q, c, (a+b)/2.0, b);
            }
        }

        static inline auto _cubic_trisect_draw(surface_p& surface, cubic_bezier_t& curve, colour_t& c, float32_t a, float32_t b) -> void {
            if (curve.versine(a, b) <= 1.0) {
                surface.draw(curve.evaluate_at(a), curve.evaluate_at(b), c);
            } else {
                _cubic_trisect_draw(surface, curve, c, a, (a+a+b)/3.0);
                _cubic_trisect_draw(surface, curve, c, (a+a+b)/3.0, (a+b+b)/3.0);
                _cubic_trisect_draw(surface, curve, c, (a+b+b)/3.0, b);
            }
        }

}

auto surface_p::draw(line_t& l, colour_t& c) -> surface_p& {
    _line_bisect_draw(*this, l, c);
    return *this;
}

auto surface_p::draw(quad_bezier_t& curve, colour_t& c) -> surface_p& {

    // float32_t current_point = 0.0;

    // while(current_point < 1.0) {
    //     // float32_t next_point = 1.0;
    //     float32_t base = (1.0 - current_point);
    //     float32_t factor = 0.5;
    //     while ((curve.versine(current_point, current_point + (base * factor)) > 1.0) && (curve.secant(current_point, current_point + (base * factor)) > 1.0))
    //     {
    //         factor *= 0.5;
    //         //asm("int3;");
    //     }
    //     draw(curve.evaluate_at(current_point), curve.evaluate_at(current_point + (base * factor)), c);
    //     // asm("int3");
    //     current_point += (base * factor);
    // }

    auto length_bound = max_norm(curve.p1 - curve.p0) + max_norm(curve.p2 - curve.p1);
    auto nsegs = length_bound / 30.0;
    // nsegs = ceil(sqrt(nsegs * nsegs * 0.6 + 100));

    auto delta = 1.0/nsegs;

    // for (auto i = 0.0; i < 1.0; i += delta)
    // {
    //     draw(curve.evaluate_at(i), curve.evaluate_at(i + delta), c);
    // }

    for (auto i = 0; i < nsegs; ++i) {
        draw(curve.evaluate_at(i * delta), curve.evaluate_at((i + 1) * delta), c);
    }

    return *this;
}

auto surface_p::draw(cubic_bezier_t& curve, colour_t& c) -> surface_p& {
    _cubic_trisect_draw(*this, curve, c, 0.0, 1.0);
    return *this;
}