#include <io/gitlab/twrl/conurbation/graphics/primitives>

using namespace io::gitlab::twrl::conurbation::graphics;

auto line_t::evaluate_at(float32_t t) const noexcept -> point_t {
    return (t * p0) + ((1-t) * p1);
}



auto cubic_bezier_t::evaluate_at(float32_t t) const noexcept -> point_t {

    return (t * t * t * p0) + (3 * t * t * (1 - t) * p1) + (3 * t * (1 - t) * (1 - t) * p2) + ((1 - t) * (1 - t) * (1 - t) * p3);

}

auto cubic_bezier_t::versine(float32_t a, float32_t b) const noexcept -> float32_t {
    float32_t m = (a + b) / 2;
    point_t A = this->evaluate_at(a);
    point_t B = this->evaluate_at(b);
    point_t M = this->evaluate_at(m);
    point_t S = midpoint(A, B);
    return max_norm(M - S);

}