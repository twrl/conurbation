#include <io/gitlab/twrl/conurbation/graphics/colour>

using namespace io::gitlab::twrl::conurbation::graphics;
using namespace io::gitlab::twrl::conurbation::graphics;

auto colour_t::write_to_pixel(uint32_t* pixel, pixel_format_t& format) -> colour_t& {

    uint32_t r, g, b, a;

    r = this->red >> (16 - format.red_size);
    g = this->green >> (16 - format.green_size);
    b = this->blue >> (16 - format.blue_size);
    a = this->alpha >> (16 - format.alpha_size);

    *pixel = (r << format.red_pos) | (g << format.green_pos) | (b << format.blue_pos) | (a << format.alpha_pos);

    return *this;

}

auto colour_t::write_to_pixel(uint16_t* pixel, pixel_format_t& format) -> colour_t& {

    uint16_t r, g, b, a;

    r = this->red >> (16 - format.red_size);
    g = this->green >> (16 - format.green_size);
    b = this->blue >> (16 - format.blue_size);
    a = this->alpha >> (16 - format.alpha_size);

    *pixel = (r << format.red_pos) | (g << format.green_pos) | (b << format.blue_pos) | (a << format.alpha_pos);

    return *this;

}

auto colour_t::write_to_pixel(uint8_t* pixel, pixel_format_t& format) -> colour_t& {

    uint8_t r, g, b, a;

    r = this->red >> (16 - format.red_size);
    g = this->green >> (16 - format.green_size);
    b = this->blue >> (16 - format.blue_size);
    a = this->alpha >> (16 - format.alpha_size);

    *pixel = (r << format.red_pos) | (g << format.green_pos) | (b << format.blue_pos) | (a << format.alpha_pos);

    return *this;

}
