#include <io/gitlab/twrl/conurbation/graphics/geometry>
#include <io/gitlab/twrl/conurbation/graphics/primitives>

using namespace io::gitlab::twrl::conurbation;
using namespace io::gitlab::twrl::conurbation::graphics;

auto point_t::normalise() const -> point_t {
    if (w == 0) return *this;
    else return point_t(v/w);
}