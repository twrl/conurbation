#include <io/gitlab/twrl/conurbation/math>
#include "words.h"

using namespace io::gitlab::twrl::conurbation;

template <> auto ::io::gitlab::twrl::conurbation::abs<float64_t>(float64_t x) noexcept -> float64_t {
    uint32_t high;
    GET_HIGH_WORD(high,x);
    SET_HIGH_WORD(x,high&0x7fffffff);
        return x;
}

template <> auto ::io::gitlab::twrl::conurbation::abs<float32_t>(float32_t x) noexcept -> float32_t {
    uint32_t ix;
    GET_FLOAT_WORD(ix,x);
    SET_FLOAT_WORD(x,ix&0x7fffffff);
        return x;
}