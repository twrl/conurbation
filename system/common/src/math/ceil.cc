// Based on code from OpenLibM, which in turn comes from ...

/* @(#)e_exp.c 1.6 04/04/22 */
/*
 * ====================================================
 * Copyright (C) 2004 by Sun Microsystems, Inc. All rights reserved.
 *
 * Permission to use, copy, modify, and distribute this
 * software is freely granted, provided that this notice 
 * is preserved.
 * ====================================================
 */


#include <io/gitlab/twrl/conurbation/math>

#include "words.h"

using namespace io::gitlab::twrl::conurbation;


static const float64_t hugesgldbl = 1.0e300;

float64_t io::gitlab::twrl::conurbation::ceil(float64_t x) noexcept
{
    int32_t i0,i1,j0;
    uint32_t i,j;
    EXTRACT_WORDS(i0,i1,x);
    j0 = ((i0>>20)&0x7ff)-0x3ff;
    if(j0<20) {
        if(j0<0) {  /* raise inexact if x != 0 */
        if(hugesgldbl+x>0.0) {/* return 0*sign(x) if |x|<1 */
            if(i0<0) {i0=0x80000000;i1=0;}
            else if((i0|i1)!=0) { i0=0x3ff00000;i1=0;}
        }
        } else {
        i = (0x000fffff)>>j0;
        if(((i0&i)|i1)==0) return x; /* x is integral */
        if(hugesgldbl+x>0.0) {    /* raise inexact flag */
            if(i0>0) i0 += (0x00100000)>>j0;
            i0 &= (~i); i1=0;
        }
        }
    } else if (j0>51) {
        if(j0==0x400) return x+x;   /* inf or NaN */
        else return x;      /* x is integral */
    } else {
        i = ((uint32_t)(0xffffffff))>>(j0-20);
        if((i1&i)==0) return x; /* x is integral */
        if(hugesgldbl+x>0.0) {        /* raise inexact flag */
        if(i0>0) {
            if(j0==20) i0+=1;
            else {
            j = i1 + (1<<(52-j0));
            if(j<i1) i0+=1; /* got a carry */
            i1 = j;
            }
        }
        i1 &= (~i);
        }
    }
    INSERT_WORDS(x,i0,i1);
    return x;
}

static const float32_t hugesgl = 1.0e30;

float32_t io::gitlab::twrl::conurbation::ceil(float32_t x) noexcept
{
    int32_t i0,j0;
    uint32_t i;

    GET_FLOAT_WORD(i0,x);
    j0 = ((i0>>23)&0xff)-0x7f;
    if(j0<23) {
        if(j0<0) {  /* raise inexact if x != 0 */
        if(hugesgl+x>(float)0.0) {/* return 0*sign(x) if |x|<1 */
            if(i0<0) {i0=0x80000000;}
            else if(i0!=0) { i0=0x3f800000;}
        }
        } else {
        i = (0x007fffff)>>j0;
        if((i0&i)==0) return x; /* x is integral */
        if(hugesgl+x>(float)0.0) { /* raise inexact flag */
            if(i0>0) i0 += (0x00800000)>>j0;
            i0 &= (~i);
        }
        }
    } else {
        if(j0==0x80) return x+x;    /* inf or NaN */
        else return x;      /* x is integral */
    }
    SET_FLOAT_WORD(x,i0);
    return x;
}