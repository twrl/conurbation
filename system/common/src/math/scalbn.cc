// Based on code from OpenLibM, which in turn comes from ...

/* @(#)e_exp.c 1.6 04/04/22 */
/*
 * ====================================================
 * Copyright (C) 2004 by Sun Microsystems, Inc. All rights reserved.
 *
 * Permission to use, copy, modify, and distribute this
 * software is freely granted, provided that this notice 
 * is preserved.
 * ====================================================
 */


#include <io/gitlab/twrl/conurbation/math>

#include "words.h"

using namespace io::gitlab::twrl::conurbation;

static const float64_t
two54dbl   =  1.80143985094819840000e+16, /* 0x43500000, 0x00000000 */
twom54dbl  =  5.55111512312578270212e-17, /* 0x3C900000, 0x00000000 */
hugedbl   = 1.0e+300,
tinydbl   = 1.0e-300;

float64_t
io::gitlab::twrl::conurbation::scalbn (float64_t x, int32_t n) noexcept
{
    int32_t k,hx,lx;
    EXTRACT_WORDS(hx,lx,x);
        k = (hx&0x7ff00000)>>20;        /* extract exponent */
        if (k==0) {             /* 0 or subnormal x */
            if ((lx|(hx&0x7fffffff))==0) return x; /* +-0 */
        x *= two54dbl;
        GET_HIGH_WORD(hx,x);
        k = ((hx&0x7ff00000)>>20) - 54;
            if (n< -50000) return tinydbl*x;   /*underflow*/
        }
        if (k==0x7ff) return x+x;       /* NaN or Inf */
        k = k+n;
        if (k >  0x7fe) return hugedbl*copysign(hugedbl,x); /* overflow  */
        if (k > 0)              /* normal result */
        {SET_HIGH_WORD(x,(hx&0x800fffff)|(k<<20)); return x;}
        if (k <= -54) {
            if (n > 50000)  /* in case integer overflow in n+k */
        return hugedbl*copysign(hugedbl,x);   /*overflow*/
        else return tinydbl*copysign(tinydbl,x);  /*underflow*/
        } 
    k += 54;                /* subnormal result */
    SET_HIGH_WORD(x,(hx&0x800fffff)|(k<<20));
        return x*twom54dbl;
}


static const float32_t
two25sgl   =  3.355443200e+07, /* 0x4c000000 */
twom25sgl  =  2.9802322388e-08,    /* 0x33000000 */
hugesgl   = 1.0e+30,
tinysgl   = 1.0e-30;

float32_t
io::gitlab::twrl::conurbation::scalbn (float32_t x, int32_t n) noexcept
{
    int32_t k,ix;
    GET_FLOAT_WORD(ix,x);
        k = (ix&0x7f800000)>>23;        /* extract exponent */
        if (k==0) {             /* 0 or subnormal x */
            if ((ix&0x7fffffff)==0) return x; /* +-0 */
        x *= two25sgl;
        GET_FLOAT_WORD(ix,x);
        k = ((ix&0x7f800000)>>23) - 25;
            if (n< -50000) return tinysgl*x;   /*underflow*/
        }
        if (k==0xff) return x+x;        /* NaN or Inf */
        k = k+n;
        if (k >  0xfe) return hugesgl*copysign(hugesgl,x); /* overflow  */
        if (k > 0)              /* normal result */
        {SET_FLOAT_WORD(x,(ix&0x807fffff)|(k<<23)); return x;}
        if (k <= -25) {
            if (n > 50000)  /* in case integer overflow in n+k */
        return hugesgl*copysign(hugesgl,x);  /*overflow*/
        else return tinysgl*copysign(tinysgl,x); /*underflow*/
        }
    k += 25;                /* subnormal result */
    SET_FLOAT_WORD(x,(ix&0x807fffff)|(k<<23));
        return x*twom25sgl;
}