// Based on code from OpenLibM, which in turn comes from ...

/* @(#)e_exp.c 1.6 04/04/22 */
/*
 * ====================================================
 * Copyright (C) 2004 by Sun Microsystems, Inc. All rights reserved.
 *
 * Permission to use, copy, modify, and distribute this
 * software is freely granted, provided that this notice 
 * is preserved.
 * ====================================================
 */


#include <io/gitlab/twrl/conurbation/math>

#include "words.h"

using namespace io::gitlab::twrl::conurbation;

float64_t io::gitlab::twrl::conurbation::copysign(float64_t x, float64_t y) noexcept
{
    uint32_t hx,hy;
    GET_HIGH_WORD(hx,x);
    GET_HIGH_WORD(hy,y);
    SET_HIGH_WORD(x,(hx&0x7fffffff)|(hy&0x80000000));
        return x;
}

float32_t io::gitlab::twrl::conurbation::copysign(float32_t x, float32_t y) noexcept
{
    uint32_t ix,iy;
    GET_FLOAT_WORD(ix,x);
    GET_FLOAT_WORD(iy,y);
    SET_FLOAT_WORD(x,(ix&0x7fffffff)|(iy&0x80000000));
        return x;
}