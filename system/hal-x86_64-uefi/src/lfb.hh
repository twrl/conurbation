#pragma once

#include <io/gitlab/twrl/conurbation/graphics/colour>
#include <io/gitlab/twrl/conurbation/graphics/surface>
#include <io/gitlab/twrl/conurbation/scalars>

#include "limine_protocol.hh"

using namespace io::gitlab::twrl::conurbation;
using namespace io::gitlab::twrl::conurbation::graphics;

namespace io::gitlab::twrl::conurbation::kernel {

class lfb_t final : public surface_p {
public:

    lfb_t(io::gitlab::twrl::conurbation::kernel::limine_framebuffer* fb);

    virtual auto draw(point_t p, colour_t& c) -> void {
        // static bool firstpixel = false;
        int64_t x = min(max(static_cast<int64_t>(p.x), width_), 0L);
        int64_t y = min(max(static_cast<int64_t>(p.y), height_), 0L);
        uintptr_t px = base_ + (x * bytes_per_pixel_) + (y * bytes_per_line_);
        // if (firstpixel) 
        //     asm("int3;"); 
        // else firstpixel = true;
        c.write_to_pixel(reinterpret_cast<uint32_t*>(px), pixelfmt_);
        //return *this;
    }

private:
    pixel_format_t pixelfmt_;

    int64_t width_, height_, bytes_per_line_, bytes_per_pixel_;

    uintptr_t base_;

};

}