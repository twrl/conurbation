[BITS 64]
[SECTION .text]

[GLOBAL _start:function (_start.end - _start)]
[EXTERN do_start]
_start:
    mov rax, cr0
    and ax, 0xFFFB      ;clear coprocessor emulation CR0.EM
    or ax, 0x2          ;set coprocessor monitoring  CR0.MP
    mov cr0, rax
    mov rax, cr4
    or ax, 3 << 9       ;set CR4.OSFXSR and CR4.OSXMMEXCPT at the same time
    mov cr4, rax
    ; mov rsp, _initstack.top

    ; mov rax, do_start
    ; push rax

    ; ret

    call do_start 

    .hcf:
    cli
    hlt
    jmp .hcf
    .end:

; [SECTION .bss]
; [GLOBAL _initstack:data _initstack.top - _initstack]
;     _initstack: 
;         resb (1024 * 1024 * 32);
;         .top:
