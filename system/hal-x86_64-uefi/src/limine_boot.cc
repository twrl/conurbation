#include "limine_protocol.hh"

using namespace io::gitlab::twrl::conurbation::kernel;

extern "C" auto _start [[noreturn]] () noexcept -> void;

namespace io::gitlab::twrl::conurbation::kernel::limine {

    limine_kernel_address_request address = {
        .id = LIMINE_KERNEL_ADDRESS_REQUEST,
        .revision = 0, .response = nullptr
    };

    limine_entry_point_request entry_point = {
        .id = LIMINE_ENTRY_POINT_REQUEST,
        .revision = 0, .response = nullptr,

        .entry = &_start
    };

    limine_framebuffer_request framebuffer = {
        .id = LIMINE_FRAMEBUFFER_REQUEST,
        .revision = 0, .response = nullptr
    };

    limine_bootloader_info_request bootloader_info = {
        .id = LIMINE_BOOTLOADER_INFO_REQUEST,
        .revision = 0, .response = nullptr
    };

    limine_hhdm_request hhdm = {
        .id = LIMINE_HHDM_REQUEST,
        .revision = 0, .response = nullptr
    };


}



namespace {

    __attribute__((section(".limine_reqs")))
    void *entry_point_req = &limine::entry_point;

    __attribute__((section(".limine_reqs")))
    void *framebuffer_req = &limine::framebuffer;

    __attribute__((section(".limine_reqs")))
    void* info_req = &limine::bootloader_info;

    __attribute__((section(".limine_reqs")))
    void* hhdm_req = &limine::hhdm;

    __attribute__((section(".limine_reqs")))
    void* addr_req = &limine::address;

}

