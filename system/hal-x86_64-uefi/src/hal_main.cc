#include <io/gitlab/twrl/conurbation/scalars>
#include <io/gitlab/twrl/conurbation/kernel/hal>
#include <io/gitlab/twrl/conurbation/kernel/memory/physical>
#include <io/gitlab/twrl/conurbation/kernel/memory/virtual>

#include "lfb.hh"
#include "limine_boot.hh"

using namespace io::gitlab::twrl::conurbation::kernel;
using namespace io::gitlab::twrl::conurbation;

auto exec_main() -> void;

static inline void outb(uint16_t port, uint8_t val)
{
    asm volatile ( "outb %0, %1" : : "a"(val), "Nd"(port) );
    /* There's an outb %al, $imm8  encoding, for compile-time constant port numbers that fit in 8b.  (N constraint).
     * Wider immediate constants would be truncated at assemble-time (e.g. "i" constraint).
     * The  outb  %al, %dx  encoding is the only option for all other cases.
     * %1 expands to %dx because  port  is a uint16_t.  %w1 could be used if we had the port number a wider C type */
}

static inline uint8_t inb(uint16_t port)
{
    uint8_t ret;
    asm volatile ( "inb %1, %0"
                   : "=a"(ret)
                   : "Nd"(port) );
    return ret;
}

static int is_transmit_empty() {
   return inb(0x3f8 + 5) & 0x20;
}
 
static void write_serial(char a) {
   while (is_transmit_empty() == 0);
 
   outb(0x3f8,a);
}

void* operator new(size_t s, void* p) { return p; }

auto hal_main [[noreturn]] () noexcept -> void {

    uint32_t log_buffer_size;

    hal_t* HAL = reinterpret_cast<hal_t*>(__builtin_alloca(sizeof(hal_t)));

    surface_p* LFB = reinterpret_cast<lfb_t*>(__builtin_alloca(sizeof(lfb_t)));
    LFB = new (LFB) lfb_t(limine::framebuffer.response->framebuffers[0]);

    // // Extract useful information from the KBoot Tags
    // kboot_tag_t* tag = reinterpret_cast<kboot_tag_t*>(tags);
    // size_t tags_size = sizeof(kboot_tag_core_t);
    // size_t i = 0;
    // while (i < tags_size) {
    //     switch (tag->type) {
    //         case kboot_tag_type_t::core:
    //             tags_size = reinterpret_cast<kboot_tag_core_t*>(tag)->tags_size;
    //             break;
    //         case kboot_tag_type_t::option:
    //             break;
    //         case kboot_tag_type_t::memory:
    //             break;
    //         case kboot_tag_type_t::vmem:
    //             break;
    //         case kboot_tag_type_t::pagetables:
    //             break;
    //         case kboot_tag_type_t::module:
    //             break;
    //         case kboot_tag_type_t::video:
    //             LFB = new (LFB) lfb_t(reinterpret_cast<kboot_tag_video_t*>(tag));
    //             break;
    //         case kboot_tag_type_t::bootdev:
    //             break;
    //         case kboot_tag_type_t::log:
    //             log = reinterpret_cast<kboot_log_t*>(reinterpret_cast<kboot_tag_log_t*>(tag)->log_virt);
    //             log_buffer_size = reinterpret_cast<kboot_tag_log_t*>(tag)->log_size - sizeof(kboot_log_t);
    //             break;
    //         case kboot_tag_type_t::sections:
    //             break;
    //         case kboot_tag_type_t::efi:
    //             break;
    //         case kboot_tag_type_t::none:
    //             goto tags_done;
    //             break;
    //         default:
    //             break;
    //     }
    //     i += (tag->size + 7) & 0xFFFFFFFFFFFFFFF8;
    //     tag = reinterpret_cast<kboot_tag_t*>(tags + i);
    // }
    // tags_done:


    // Set up memory manager(s)

    // Set up ISRs

    // Parse UEFI and ACPI tables for information

    // Configure APICs

    // Enumerate buses

    // Configure framebuffer


    // lfb_t lfb{};

    LFB->draw(point_t(0, 0), point_t(100.0, 100.0), point_t(100.0, 300.0), colour_t(uint16_t(0xFFFF), uint16_t(0xFFFF), uint16_t(0)));



    exec_main();

    asm("cli; hlt;");
    __builtin_unreachable();

}

