#pragma once

#include "limine_protocol.hh"

namespace io::gitlab::twrl::conurbation::kernel::limine {

    extern limine_kernel_address_request address;
    extern limine_entry_point_request entry_point;
    extern limine_framebuffer_request framebuffer;
    extern limine_bootloader_info_request bootloader_info;
    extern limine_hhdm_request hhdm;

}