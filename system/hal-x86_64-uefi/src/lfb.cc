#include "lfb.hh"

using namespace io::gitlab::twrl::conurbation::kernel;

lfb_t::lfb_t(io::gitlab::twrl::conurbation::kernel::limine_framebuffer* fb) :
    pixelfmt_{ static_cast<uint8_t>(fb->bpp), fb->red_mask_size, fb->red_mask_shift, fb->green_mask_size, fb->green_mask_shift, fb->blue_mask_size, fb->blue_mask_shift, 0, 0 },
    width_(fb->width),
    height_(fb->height),
    bytes_per_line_(fb->pitch),
    bytes_per_pixel_((fb->bpp + 7) / 8),
    base_(reinterpret_cast<uintptr_t>(fb->address))
    {}