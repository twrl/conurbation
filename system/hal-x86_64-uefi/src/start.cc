#include <io/gitlab/twrl/conurbation/scalars>
#include <io/gitlab/twrl/conurbation/elf64>
#include <io/gitlab/twrl/conurbation/kernel/kboot>

#include "limine_boot.hh"

using namespace io::gitlab::twrl::conurbation;

// extern "C" uint8_t KERNEL_VMA;
extern "C" uint64_t _GLOBAL_OFFSET_TABLE_[];

extern "C" elf_dyn_t _DYNAMIC[];

using initfn_f = auto () -> void;

auto hal_main [[noreturn]] () noexcept -> void;

extern "C" auto do_start [[noreturn]] () noexcept -> void {

    

    // Being a kernel, we can safely assume that our base address is 0
    uintptr_t base_address = kernel::limine::address.response->virtual_base;

    // Get the location of the DYN section
    // elf_ehdr_t* EHDR = reinterpret_cast<elf_ehdr_t*>(base_address + &KERNEL_VMA);
    elf_dyn_t* DYN = _DYNAMIC;

    // for (int i = 0; i < EHDR->e_phnum; i += EHDR->e_phentsize)
    // {
    //     elf_phdr_t* PHDR = reinterpret_cast<elf_phdr_t*>(&KERNEL_VMA + EHDR->e_phoff + i);
    //     if (PHDR->p_type == elf_phdr_type_t::PT_DYNAMIC) {
    //         DYN = reinterpret_cast<elf_dyn_t*>(PHDR->p_vaddr + base_address);
    //     }
    // }

    // Extract these values from the DYN section
    uint64_t rela_dyn_base;
    uint64_t rela_dyn_size = 0;
    uint64_t rela_dyn_entsize;
    uint64_t preinit_array_base;
    uint64_t preinit_array_size = 0;
    uint64_t init_array_base;
    uint64_t init_array_size = 0;

    for (int i = 0; DYN[i].d_tag != elf_dynamic_tag_t::DT_NULL; ++i)
    {
        switch (DYN[i].d_tag) {
            case elf_dynamic_tag_t::DT_RELA:
                rela_dyn_base = DYN[i].d_val;
                break;
            case elf_dynamic_tag_t::DT_RELASZ:
                rela_dyn_size = DYN[i].d_val;
                break;
            case elf_dynamic_tag_t::DT_RELAENT:
                rela_dyn_entsize = DYN[i].d_val;
                break;
            case elf_dynamic_tag_t::DT_INIT_ARRAY:
                init_array_base = DYN[i].d_val;
                break;
            case elf_dynamic_tag_t::DT_INIT_ARRAYSZ:
                init_array_size = DYN[i].d_val;
                break;
            case elf_dynamic_tag_t::DT_PREINIT_ARRAY:
                preinit_array_base = DYN[i].d_val;
                break;
            case elf_dynamic_tag_t::DT_PREINIT_ARRAYSZ:
                preinit_array_size = DYN[i].d_val;
                break;
            case elf_dynamic_tag_t::DT_NULL:
                goto doneDYN;
            default:
                continue;
        }
    }
    doneDYN:

    // Apply relocations to self
    if (rela_dyn_size > 1) {
        for (int i = 0; i < rela_dyn_size; i += rela_dyn_entsize)
        {
            elf_rela_t* R = reinterpret_cast<elf_rela_t*>(rela_dyn_base + i);
            switch (R->type()) {
                case elf_relocation_type_t::R_AMD64_RELATIVE:
                    { uint64_t* const target = R->target<uint64_t>();
                    *target = base_address + R->r_addend; }
                    break;
                default:
                    // Ignore unknown Rela types - should this be a panic?
                    break;
            }
        }
    }

    // TODO: _GLOBAL_OFFSET_TABLE_ plumbing


    // Run init and preinit arrays
    if (preinit_array_size > 1) {
        for (int i = 0; i < preinit_array_size; i += sizeof(initfn_f*))
        {
            initfn_f** F = reinterpret_cast<initfn_f**>(preinit_array_base + i);
            (*F)();
        }
    }

    if (init_array_size > 1) {
        for (int i = 0; i < init_array_size; i += sizeof(initfn_f*))
        {
            initfn_f** F = reinterpret_cast<initfn_f**>(init_array_base + i);
            (*F)();
        }
    }

    // Start doing something useful...

    hal_main();

}


