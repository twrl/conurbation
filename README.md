# Conurbation

[![EUPL-1.1](https://img.shields.io/badge/license-EUPL--1%2C1-blue?style=flat-square)](https://joinup.ec.europa.eu/software/page/eupl5) [![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen?style=flat-square)](http://commitizen.github.io/cz-cli/)

Conurbation is my [hobby Operating System](http://wiki.osdev.org), and this repository is it's latest incarnation. Still very much a :construction: work in progress.


## :fast_forward: Quick Start

Conurbation doesn't support in-tree builds. This quick start assumes you're going to create a folder called 'build' in the root of the project directory, but do what you like...

```bash
~/Conurbation $ mkdir build
~/Conurbation $ cd build 
~/Conurbation/build $ ../configure
~/Conurbation/build $ ninja
~/Conurbation/build $ ./simulate 
```

That was difficult, wasn't it? 


## :small_red_triangle_down: Prerequisites

You will need:

  * LLVM and clang, version 8.0 or better
  * NASM 2.10 or better
  * Ninja, version 1.8 or better
  * A bash shell and the usual utilities
  * QEMU (_optional, for testing_)


## :books: Documentation

Documentation can be found in the aptly named [documentation](documentation) submodule, or through the [Gitlab Wiki](//gitlab.com/twrl/conurbation/wikis/home).


## :copyright: License

> Licensed under the EUPL, Version 1.1 only (the "Licence");
> You may not use this work except in compliance with the Licence.
> You may obtain a copy of the Licence at:
> 
>   https://joinup.ec.europa.eu/software/page/eupl5
> 
> Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
> 
> See the Licence for the specific language governing permissions and limitations under the Licence.

My code is licensed under EUPL-1.1 as above. Other people's code may not be. 


## :ok_hand: Thanks

Thanks to:

  * [Alex Smith](https://github.com/aejsmith) for his [KBoot bootloader](https://github.com/aejsmith/kboot)
  * [Gerd Hoffmann](https://www.kraxel.org/blog/about/) for building [OVMF](https://github.com/tianocore/tianocore.github.io/wiki/OVMF) so the rest of us don't have to
  * [OpenLibm](https://openlibm.org) for base mathematical functions
  * [Emery Berger](http://www.emeryberger.com/) and [The PLASMA Group](https://plasma-umass.org) for [Heap Layers](https://github.com/emeryberger/Heap-Layers), [Mesh](https://github.com/plasma-umass/Mesh), and general awesomeness.
  * All the lovely contributors and community at [osdev.org](http://wiki.osdev.org) and [bona fide os developer](http://osdever.net)
  * Too many osdev and other projects to name, for inspiration
